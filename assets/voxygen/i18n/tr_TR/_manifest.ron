/// Translation document instructions
///
/// In order to keep localization documents readible please follow the following
/// rules:
/// - separate the string map sections using a commentary describing the purpose
///   of the next section
/// - prepend multi-line strings with a commentary
/// - append one blank lines after a multi-line strings and two after sections
///
/// To add a new language in Veloren, just write an additional `.ron` file in
/// `assets/voxygen/i18n` and that's it!
///
/// WARNING: Localization files shall be saved in UTF-8 format without BOM

/// Localization for Turkish (Turkey)
(
    metadata: (
        language_name: "Türkçe (Türkiye)",
        language_identifier: "tr_TR",
    ),
    convert_utf8_to_ascii: false,
    fonts: {
        "opensans": Font (
            asset_key: "voxygen.font.OpenSans-Regular",
            scale_ratio: 1.0,
        ),
        "metamorph": Font (
            asset_key: "voxygen.font.Metamorphous-Regular",
            scale_ratio: 1.0,
        ),
        "alkhemi": Font (
            asset_key: "voxygen.font.Alkhemikal",
            scale_ratio: 1.0,
        ),
        "wizard": Font (
            asset_key: "voxygen.font.wizard",
            scale_ratio: 1.0,
        ),
        "cyri": Font (
            asset_key: "voxygen.font.haxrcorp_4089_cyrillic_altgr_extended",
            scale_ratio: 1.0,
        ),
    },
    sub_directories: [],
    string_map: {
        /// Start Common section
        // Texts used in multiple locations with the same formatting
        "common.username": "kullanıcı adı",
        "common.singleplayer": "Tek oyuncu",
        "common.multiplayer": "Çok oyunculu",
        "common.servers": "Sunucular",
        "common.quit": "Çık",
        "common.settings": "Ayarlar",
        "common.languages": "Diller",
        "common.interface": "Arayüz",
        "common.gameplay": "Oynanış",
        "common.controls": "Kontroller",
        "common.video": "Video",
        "common.sound": "Ses",
        "common.languages": "Diller",
        "common.resume": "Devam Et",
        "common.characters": "Karakterler",
        "common.close": "Kapat",
        "common.yes": "Evet",
        "common.no": "Hayır",
        "common.back": "Geri",
        "common.create": "Oluştur",
        "common.okay": "Tamam",
        "common.add": "Ekle",
        "common.accept": "Kabul Et", 
        "common.decline": "Reddet",
        "common.disclaimer": "Uyarı",
        "common.cancel": "İptal Et",
        "common.none": "Yok",
        "common.error": "Hata",
        "common.fatal_error": "Ölümcül hata",
        "common.you": "Sen",
        "common.automatic": "Otomatik",
        "common.random": "Rastgele",
        // Settings Window title
        "common.interface_settings": "Arayüz Ayarları",
        "common.gameplay_settings": "Oynanış Ayarları",
        "common.controls_settings": "Kontrol Ayarları",
        "common.video_settings": "Görüntü Ayarları",
        "common.sound_settings": "Ses Ayarları",
        "common.language_settings": "Dil Ayarları",

        // Message when connection to the server is lost
        "common.connection_lost": r#"Bağlantı koptu!
Sunucu yeniden mi başladı?
İstemci güncel mi?"#,


        "common.species.orc": "Ork",
        "common.species.human": "İnsan",
        "common.species.dwarf": "Cüce",
        "common.species.elf": "Elf",
        "common.species.undead": "Hortlak",
        "common.species.danari": "Danari",

        "common.weapons.axe": "Balta",
        "common.weapons.sword": "Kılıç",
        "common.weapons.staff": "Asa",
        "common.weapons.bow": "Yay",
        "common.weapons.hammer": "Çekiç",
        "common.weapons.sceptre": "Şifa Asası",
        "common.rand_appearance": "Rastgele görünüm ve isim",
        /// End Common section


        /// Start Main screen section
        "main.username": "Kullanıcı adı",
        "main.server": "Sunucu",
        "main.password": "Şifre",
        "main.connecting": "Bağlanılıyor", 
        "main.creating_world": "Dünya oluşturuluyor",
        "main.tip": "İpucu:",

        // Welcome notice that appears the first time Veloren is started
        "main.notice": r#"Veloren Alfa sürümüne hoşgeldin!

Eğlenmeye başlamadan önce, lütfen bir kaç şeyi aklında tut:

- Bu alfa sürümü daha çok yeni. Hatalar, bitmemiş oynanış, elden geçirilmemiş mekanikler ve eksik özellikler bulunuyor.

- Yapıcı geri bildirim veya hata raporların varsa bize Reddit, GitLab veya Discord kullanarak ulaşabilirsin.

- Veloren GPL 3 açık kaynak lisansı ile lisanslıdır. Bunun anlamı, oyunu istediğin gibi oynayabilir, değiştirebilir ve dağıtabilirsin
(türetilmiş çalışmalarda GPL 3 ile lisanslanmış olduğu sürece)

- Veloren kar gütmeyen bir topluluk projesidir ve üzerinde çalışan herkes birer gönüllüdür.
Gördüklerini beğendiysen, geliştirme veya sanat takımlarına katılabilirsin!

Bu bildiriyi okumaya zaman ayırdığın için teşekkür ederiz, umarız ki oyundan memnun kalırsın!

~ Veloren Geliştiricileri"#,

        // Login process description
        "main.login_process": r#"Giriş işlemi hakkında bilgi:

Eğer giriş yaparken sorunlarla karşılaşıyorsan:

Lütfen kimlik doğrulaması gerektiren sunucularda
oynamak için bir hesaba ihtiyacın olduğunu hatırla.

https://veloren.net/account/ adresinden

bir hesap oluşturabilirsin."#,
        "main.login.server_not_found": "Sunucu bulunamadı",
        "main.login.authentication_error": "Sunucuda kimlik doğrulama hatası",
        "main.login.server_full": "Sunucu dolu",
        "main.login.untrusted_auth_server": "Kimlik doğrulama sunucusu güvenilir değil",
        "main.login.outdated_client_or_server": "SunucuÇılgınaDöndü: Muhtemelen versiyonlar uyuşmuyor, güncellemeler için kontrol et.",
        "main.login.timeout": "Zamanaşımı: Sunucu zamanında cevap vermedi. (Aşırı yüklenme veya ağ sorunları).",
        "main.login.server_shut_down": "Sunucu kapandı",
        "main.login.already_logged_in": "Zaten sunucuya giriş yapmışsın.",
        "main.login.network_error": "Ağ hatası",
        "main.login.failed_sending_request": "Kimlik doğrulama sunucusuna istek gönderilemedi",
        "main.login.invalid_character": "Seçilen karakter geçersiz",
        "main.login.client_crashed": "İstemci çöktü",
        "main.login.not_on_whitelist": "Sunucuya girmek için bir Yönetici tarafından beyaz listeye eklenmen gerekiyor",
        "main.login.banned": "Sunucudan aşağıdaki sebepten dolayı yasaklandınız",
        "main.login.kicked": "Sunucudan aşağıdaki sebepten dolayı atıldınız",
        "main.login.select_language": "Bir dil seç",
        
        "main.servers.select_server": "Bir sunucu seç",

        /// End Main screen section


        /// Start HUD Section
        "hud.do_not_show_on_startup": "Bunu açılışta gösterme",
        "hud.show_tips": "Öneriler",
        "hud.quests": "Görevler",
        "hud.you_died": "Öldün",
        "hud.waypoint_saved": "Yol noktası kaydedildi",

        "hud.press_key_to_show_keybindings_fmt": "Kontrolleri göstermek için {key}'e bas",
        "hud.press_key_to_toggle_lantern_fmt": "Fenerini yakmak veya söndürmek için [{key}]'e bas", 
        "hud.press_key_to_show_debug_info_fmt": "Hata ayıklama bilgilerini göstermek için {key}'e bas",
        "hud.press_key_to_toggle_keybindings_fmt": "Kontrolleri açmak veya kapamak için {key}'e bas",
        "hud.press_key_to_toggle_debug_info_fmt": "Hata ayıklama bilgilerini açmak veya kapamak için {key}'e bas",

        // Chat outputs
        "hud.chat.online_msg": "[{name}] çevrimiçi oldu.",
        "hud.chat.offline_msg": "[{name}] çevrimdışı oldu.",

        "hud.chat.default_death_msg": "[{name}] öldü.",
        "hud.chat.environmental_kill_msg": "[{name}] {environment}'da öldü.",
        "hud.chat.fall_kill_msg": "[{name}] yüksekten düşerek öldü.",
        "hud.chat.suicide_msg": "[{name}] kendini yaralayarak öldü.",

        "hud.chat.pvp_melee_kill_msg": "[{victim}], [{attacker}] tarafından mağlup edildi.",
        "hud.chat.pvp_ranged_kill_msg": "[{victim}], [{attacker}] tarafından vuruldu.",
        "hud.chat.pvp_explosion_kill_msg": "[{victim}], [{attacker}] tarafından havaya uçuruldu.",
        "hud.chat.pvp_energy_kill_msg": "[{victim}], [{attacker}] tarafından büyü ile mağlup edildi.",
        "hud.chat.pvp_buff_kill_msg": "[{victim}], [{attacker}] tarafından öldürüldü.",


        "hud.chat.npc_melee_kill_msg": "[{victim}], [{attacker}] tarafından mağlup edildi.",
        "hud.chat.npc_ranged_kill_msg": "[{victim}], [{attacker}] tarafından vuruldu.",
        "hud.chat.npc_explosion_kill_msg": "[{victim}], [{attacker}] tarafından havaya uçuruldu.",
        "hud.chat.npc_energy_kill_msg": "[{victim}], [{attacker}] tarafından büyü ile mağlup edildi.",
        "hud.chat.npc_other_kill_msg": "[{victim}], [{attacker}] tarafından öldürüldü.",

        "hud.chat.loot_msg": "[{item}] topladın.",
        "hud.chat.loot_fail": "Envanterin dolu!",
        "hud.chat.goodbye": "Hoşçakal!",
        "hud.chat.connection_lost": "Bağlantı koptu. {time} saniye içinde sunucudan atılacaksın.",

        // SCT outputs
        "hud.sct.experience": "{amount} Tecrübe",
        "hud.sct.block": "BLOKLANDI",

        // Respawn message
        "hud.press_key_to_respawn": r#"Ziyaret ettiğin en son kamp ateşinde yeniden doğmak için {key}'e bas."#,

        // Welcome message
        "hud.welcome": r#"Veloren Alfa sürümüne hoşgeldin!,


Başlamadan önce bazı ipuçları:


EN ÖNEMLİSİ: Yeniden doğma noktanı ayarlamak için sohbete /waypoint yaz.

Bu ölmüşsen bile yapılabilir!


Kontrolleri görmek için F1'e bas.

Sohbet komutlarını görmek için sohbete /help yaz.


Dünyada rastgele oluşan sandıklar ve başka objeler var!

Onları toplamak için Sağ-Tık kullan.

Topladıklarını kullanmak için 'B' tuşuna basarak envanterini aç.

Envanterindeki eşyaları kullanmak veya kuşanmak için iki kere üzerlerine tıkla.

Üzerlerine bir kere tıklayıp ve sonra envaterin dışına tıklayarak onları at.


Veloren'de geceler oldukça karanlık olabiliyor.

'G' tuşuna basarak fenerini yak.


Bu pencereyi kapatmak için imlecini serbest bırakmak mı istiyorsun? TAB'a bas!


Veloren'in Dünyasında sana iyi eğlenceler!"#,

"hud.temp_quest_headline": r#"Lütfen bize yardım et maceracı!"#,
"hud.temp_quest_text": r#"Kasabamızın etrafında kültistlerle
dolu zindanlar belirdi.

Birkaç yoldaş bul, yiyeceğini hazırla
ve kült lideri ile onun yardımcılarını alt et.

Kim bilir? Belki onların tılsımlı
eşyalarını bile ele geçirebilirsin!"#,


        // Inventory
        "hud.bag.inventory": "{playername}'in Envanteri",
        "hud.bag.stats_title": "{playername}'in Nitelikleri",
        "hud.bag.exp": "Tecrübe",
        "hud.bag.armor": "Zırh",
        "hud.bag.stats": "Nitelikler",
        "hud.bag.head": "Baş",
        "hud.bag.neck": "Boyun",
        "hud.bag.tabard": "Cüppe",
        "hud.bag.shoulders": "Omuzlar",
        "hud.bag.chest": "Göğüs",
        "hud.bag.hands": "Eller",
        "hud.bag.lantern": "Fener",
        "hud.bag.glider": "Planör",
        "hud.bag.belt": "Kemer",
        "hud.bag.ring": "Yüzük",
        "hud.bag.back": "Arka",
        "hud.bag.legs": "Bacaklar",
        "hud.bag.feet": "Ayaklar",
        "hud.bag.mainhand": "Birincil",
        "hud.bag.offhand": "İkincil",

        // Map and Questlog
        "hud.map.map_title": "Harita",
        "hud.map.qlog_title": "Görevler",

        // Settings
        "hud.settings.general": "Genel",
        "hud.settings.none": "Yok",
        "hud.settings.press_behavior.toggle": "Aç/kapa",
        "hud.settings.press_behavior.hold": "Basılı tut",
        "hud.settings.help_window": "Yardım Penceresi",
        "hud.settings.debug_info": "Hata Ayıklama Bilgileri",
        "hud.settings.tips_on_startup": "Açılışta İpuçlarını Göster",
        "hud.settings.ui_scale": "Arayüz Ölçeği",
        "hud.settings.relative_scaling": "Otomatik Ölçek",
        "hud.settings.custom_scaling": "Sabit Ölçek",
        "hud.settings.crosshair": "İmleç tipi",
        "hud.settings.transparency": "Şeffaflık",
        "hud.settings.hotbar": "Aksiyon Çubuğu",
        "hud.settings.toggle_shortcuts": "Kısayolları aç/kapa",
        "hud.settings.buffs_skillbar": "Etkiler yetenek çubuğunun üstünde",
        "hud.settings.buffs_mmap": "Etkiler haritanın yanında",
        "hud.settings.toggle_bar_experience": "Tecrübe çubuğunu aç/kapa",
        "hud.settings.scrolling_combat_text": "Verilen/Alınan Hasar Yazısı",
        "hud.settings.single_damage_number": "Verilen Hasarı Tek Tek Göster",
        "hud.settings.cumulated_damage": "Toplam Verilen Hasarı Göster",
        "hud.settings.incoming_damage": "Alınan Hasarı Tek Tek Göster",
        "hud.settings.cumulated_incoming_damage": "Toplam Alınan Hasarı Göster",
        "hud.settings.speech_bubble": "Konuşma balonu",
        "hud.settings.speech_bubble_dark_mode": "Konuşma balonunda karanlık tema kullan",
        "hud.settings.speech_bubble_icon": "Konuşma balonunda ikon göster",
        "hud.settings.energybar_numbers": "Enerji çubuğu değerleri",
        "hud.settings.values": "Sayılar",
        "hud.settings.percentages": "Yüzdeler",
        "hud.settings.chat": "Sohbet",
        "hud.settings.background_transparency": "Arkaplan Şeffaflığı",
        "hud.settings.chat_character_name": "Sohbette karakter isimlerini göster",
        "hud.settings.loading_tips": "Yükleme ekranı ipuçları",

        "hud.settings.pan_sensitivity": "Kaydırma Hassaslığı",
        "hud.settings.zoom_sensitivity": "Büyütme Hassaslığı",
        "hud.settings.invert_scroll_zoom": "Kaydırma Büyütmesini ters çevir",
        "hud.settings.invert_mouse_y_axis": "Fare Y eksenini ters çevir",
        "hud.settings.enable_mouse_smoothing": "Kamera kontrolünü yumuşat",
        "hud.settings.free_look_behavior": "Serbest bakış davranışı",
        "hud.settings.auto_walk_behavior": "Otomatik yürüme davranışı",
        "hud.settings.stop_auto_walk_on_input": r#"Otomatik yürüyüşü hareket
edince kapat"#,

        "hud.settings.view_distance": "Görüş Mesafesi",
        "hud.settings.sprites_view_distance": "Sprite Görüş Mesafesi",
        "hud.settings.figures_view_distance": "Figür Görüş Mesafesi",
        "hud.settings.maximum_fps": "Maksimum FPS",
        "hud.settings.fov": "Görüş alanı (derece)",
        "hud.settings.gamma": "Gama",
        "hud.settings.exposure": "Görünürlük",
        "hud.settings.ambiance": "Ortam Parlaklığı", 
        "hud.settings.antialiasing_mode": "Kenar Yumuşatma Modu",
        "hud.settings.upscale_factor": "Render Büyütme Faktörü",
        "hud.settings.cloud_rendering_mode": "Bulut Kalitesi",
        "hud.settings.fluid_rendering_mode": "Su Kalitesi",
        "hud.settings.fluid_rendering_mode.cheap": "Basit",
        "hud.settings.fluid_rendering_mode.shiny": "Güzel",
        "hud.settings.cloud_rendering_mode.minimal": "En Düşük",
        "hud.settings.cloud_rendering_mode.low": "Düşük",
        "hud.settings.cloud_rendering_mode.medium": "Orta",
        "hud.settings.cloud_rendering_mode.high": "Yüksek",
        "hud.settings.cloud_rendering_mode.ultra": "En Yüksek",
        "hud.settings.fullscreen": "Tam Ekran",
        "hud.settings.fullscreen_mode": "Tam Ekran Modu",
        "hud.settings.fullscreen_mode.exclusive": "Ekranı kapla",
        "hud.settings.fullscreen_mode.borderless": "Penceresiz",
        "hud.settings.particles": "Partiküller",
        "hud.settings.resolution": "Çözünürlük",
        "hud.settings.bit_depth": "Bit Derinliği",
        "hud.settings.refresh_rate": "Yenileme Hızı",
        "hud.settings.lighting_rendering_mode": "Aydınlanma Modu",
        "hud.settings.lighting_rendering_mode.ashikhmin": "A Tipi - Yüksek",
        "hud.settings.lighting_rendering_mode.blinnphong": "B Tipi - Orta",
        "hud.settings.lighting_rendering_mode.lambertian": "L Tipi - Düşük",
        "hud.settings.shadow_rendering_mode": "Gölge Modu",
        "hud.settings.shadow_rendering_mode.none": "Yok",
        "hud.settings.shadow_rendering_mode.cheap": "Basit",
        "hud.settings.shadow_rendering_mode.map": "Ayrıntılı",
        "hud.settings.shadow_rendering_mode.map.resolution": "Çözünürlük",
        "hud.settings.lod_detail": "Uzak Cisim Detayı",
        "hud.settings.save_window_size": "Pencere boyutunu kaydet",

        "hud.settings.music_volume": "Müzik Sesi",
        "hud.settings.sound_effect_volume": "Efekt Sesi",
        "hud.settings.audio_device": "Ses Aygıtı",

        "hud.settings.awaitingkey": "Bir tuşa bas...",
        "hud.settings.unbound": "Atanmamış",
        "hud.settings.reset_keybinds": "Varsayılana döndür",

        "hud.social": "Diğer Oyuncular",
        "hud.social.online": "Çevrimiçi:",
        "hud.social.friends": "Arkadaşlar",
        "hud.social.not_yet_available": "Şu anda kullanılabilir değil",
        "hud.social.faction": "Klan",
        "hud.social.play_online_fmt": "{nb_player} oyuncu çevrimiçi",
        "hud.social.name": "İsim",
        "hud.social.level": "Seviye",
        "hud.social.zone": "Bölge",
        "hud.social.account": "Hesap",

        "hud.crafting": "Üretim",
        "hud.crafting.recipes": "Tarifler",
        "hud.crafting.ingredients": "Malzemeler:",
        "hud.crafting.craft": "Üret",
        "hud.crafting.tool_cata": "Gerektiriyor:",

        "hud.group": "Grup",
        "hud.group.invite_to_join": "{name} seni grubuna davet etti!",
        "hud.group.invite": "Davet Et",
        "hud.group.kick": "Gruptan At",
        "hud.group.assign_leader": "Lider Seç",
        "hud.group.leave": "Gruptan Ayrıl",
        "hud.group.dead" : "Ölü",
        "hud.group.out_of_range": "Erişim dışı",
        "hud.group.add_friend": "Arkadaşlara Ekle",
        "hud.group.link_group": "Grupları Bağla",
        "hud.group.in_menu": "Menüde",
        "hud.group.members": "Grup Üyeleri",

        "hud.spell": "Büyü",

        "hud.free_look_indicator": "Serbest bakış açık, kapatmak için {key} tuşuna bas",
        "hud.auto_walk_indicator": "Otomatik yürüme açık",

        "hud.map.difficulty": "Zorluk",
        "hud.map.towns": "Şehirler",
        "hud.map.castles": "Kaleler",
        "hud.map.dungeons": "Zindanlar",
        "hud.map.caves": "Mağaralar",
        "hud.map.cave": "Mağara",
        "hud.map.town": "Şehir",
        "hud.map.castle": "Kale",
        "hud.map.dungeon": "Zindan",
        "hud.map.difficulty_dungeon": "Zindan\n\nZorluk: {difficulty}",
        "hud.map.drag": "Sürükle",
        "hud.map.zoom": "Büyüt / küçült",
        "hud.map.recenter": "Merkezle",

        /// End HUD section


        /// Start GameInput section

        "gameinput.primary": "Birincil Saldırı",
        "gameinput.secondary": "İkincil Saldırı/Savun/Hedef al",
        "gameinput.slot1": "Eylem çubuğu Slot 1",
        "gameinput.slot2": "Eylem çubuğu Slot 2",
        "gameinput.slot3": "Eylem çubuğu Slot 3",
        "gameinput.slot4": "Eylem çubuğu Slot 4",
        "gameinput.slot5": "Eylem çubuğu Slot 5",
        "gameinput.slot6": "Eylem çubuğu Slot 6",
        "gameinput.slot7": "Eylem çubuğu Slot 7",
        "gameinput.slot8": "Eylem çubuğu Slot 8",
        "gameinput.slot9": "Eylem çubuğu Slot 9",
        "gameinput.slot10": "Eylem çubuğu Slot 10",
        "gameinput.swaploadout": "Tehçizatı Değiştir",
        "gameinput.togglecursor": "Fareyi aç/kapa",
        "gameinput.help": "Yardım penceresini aç/kapa",
        "gameinput.toggleinterface": "Arayüzü aç/kapa",
        "gameinput.toggledebug": "FPS ve Hata ayıklama bilgilerini aç/kapa",
        "gameinput.screenshot": "Ekran görüntüsü al",
        "gameinput.toggleingameui": "İsim etiketlerini aç/kapa",
        "gameinput.fullscreen": "Tam ekranı aç/kapa",
        "gameinput.moveforward": "İleri git",
        "gameinput.moveleft": "Sola git",
        "gameinput.moveright": "Sağa git",
        "gameinput.moveback": "Geri git",
        "gameinput.jump": "Zıpla",
        "gameinput.glide": "Planör",
        "gameinput.roll": "Yuvarlan",
        "gameinput.climb": "Tırman",
        "gameinput.climbdown": "İn",
        "gameinput.wallleap": "Duvar Sıçrayışı",
        "gameinput.togglelantern": "Feneri yak/söndür",
        "gameinput.mount": "Bin",
        "gameinput.chat": "Sohbet",
        "gameinput.command": "Komut",
        "gameinput.escape": "Oyunu Duraklat",
        "gameinput.map": "Harita",
        "gameinput.bag": "Envanter",
        "gameinput.social": "Sosyal",
        "gameinput.sit": "Otur",
        "gameinput.spellbook": "Büyüler",
        "gameinput.settings": "Ayarlar",
        "gameinput.respawn": "Yeniden Canlan",
        "gameinput.charge": "Hücum",
        "gameinput.togglewield": "Kuşan/koy",
        "gameinput.interact": "Etkileşim",
        "gameinput.freelook": "Serbest Bakış",
        "gameinput.autowalk": "Otomatik Yürüyüş",
        "gameinput.dance": "Dans et",
        "gameinput.select": "Varlık Seç",
        "gameinput.acceptgroupinvite": "Grup Davetini Kabul Et",
        "gameinput.declinegroupinvite": "Grup Davetini Reddet",
        "gameinput.crafting": "Üretim",
        "gameinput.fly": "Uç",
        "gameinput.sneak": "Eğil", 
        "gameinput.swimdown": "Aşağı yüz",
        "gameinput.swimup": "Yukarı yüz", 

        /// End GameInput section


        /// Start chracter selection section
        "char_selection.loading_characters": "Karakterler yükleniyor...",
        "char_selection.delete_permanently": r#"Bu karakteri kalıcı olarak
silmek istediğinden emin misin?"#,
        "char_selection.deleting_character": "Karakter siliniyor...",
        "char_selection.change_server": "Sunucu Değiştir",
        "char_selection.enter_world": "Dünyaya Gir",
        "char_selection.logout": "Çıkış yap",
        "char_selection.create_new_character": "Yeni Karakter Oluştur",
        "char_selection.creating_character": "Karakter oluşturuluyor...",
        "char_selection.character_creation": "Karakter Oluşturma",

        "char_selection.human_default": "İnsan Varsayılanı",
        "char_selection.level_fmt": "Seviye {level_nb}",
        "char_selection.uncanny_valley": "Esrarengiz Vadi",
        "char_selection.plains_of_uncertainty": "Belirsizlik Ovaları",
        "char_selection.beard": "Sakal",
        "char_selection.hair_style": "Saç Stili",
        "char_selection.hair_color": "Saç Rengi",
        "char_selection.eye_color": "Göz Rengi",
        "char_selection.skin": "Ten Rengi",
        "char_selection.eyeshape": "Göz Detayları",
        "char_selection.accessories": "Aksesuarlar",
        "char_selection.create_info_name": "Karakterinin bir isme ihtiyacı var!",

        /// End chracter selection section


        /// Start character window section
        "character_window.character_name": "Karakter Adı",
        // Charater stats
        "character_window.character_stats": r#"Dayanıklılık

Çeviklik

İrade gücü

Koruma
"#, 
        /// End character window section


        /// Start Escape Menu Section
        "esc_menu.logout": "Çıkış yap",
        "esc_menu.quit_game": "Oyundan çık",
        /// End Escape Menu Section

        /// Buffs and Debuffs
        "buff.remove": "Etkiyi kadırmak için tıkla",
        "buff.title.missing": "İsim Yok",
        "buff.desc.missing": "Açıklama Yok",
        // Buffs
        "buff.title.heal": "İyileşme",
        "buff.desc.heal": "Zamanla can kazan.",
        "buff.title.potion": "İksir",
        "buff.desc.potion": "İçiyor...",
        "buff.title.saturation": "Satürasyon",
        "buff.desc.saturation": "Tüketilebilen maddelerden zamanla can kazan.",
        // Debuffs
        "debuff.title.bleed": "Kanama",
        "debuff.desc.bleed": "Sıradan hasar verir.",
    },

    vector_map: {
		"loading.tips": [
			"'G'ye basarak fenerini yak.",
			"'F1'e basarak bütün kontrolleri görebilirsin.",
			"'/say' veya '/s' yazarak sadece hemen yanındaki oyuncularla konuşabilirsin.",
			"'/region' veya '/r' yazarak sadece bir kaç yüz blok içindeki oyuncularla konuşabilirsin.",
			"Özel bir mesaj göndermek için '/tell' ve sonra bir oyuncu ismi ile mesajını yaz.",
			"Aynı seviyedeki NPCler farklı zorluklara sahip olabilir.",
			"Yemek, sandık ve diğer ganimetler için yere bak!",
			"Envanterin yemekle mi dolu? Onları kullanarak daha iyi yemek yapmaya çalış!",
			"Ne yapabileceğini merak mı ediyorsun? Zindanlar haritada kahverengi bölgeler olarak işaretlenmiştir!",
			"Grafikleri sistemin için ayarlamayı unutma. 'N'e basarak ayarları aç.",
			"Başkalarıyla oynamak eğlencelidir! 'O'ya basarak kimlerin çevirimiçi olduğunu gör.",
			"Can barının yanında kurukafa olan bir NPC senden hayli bir güçlüdür.",
			"'J'ye basarak dans et. Parti!",
			"'L-Shift'e basarak Planörünü aç ve gökyüzünü fethet.",
			"Veloren hala Pre-Alpha'da. Onu geliştirmek için her gün elimizden geleni yapıyoruz!",
			"Geliştirme Takımına katılmak istiyorsan veya sadece sohbet etmek istiyorsan Discord sunucumuza katıl.",
			"Can barında canı sayı olarak görmek istiyorsan, bunu ayarlardan aktifleştirebilirsin.",
			"Niteliklerini görmek için envanterindeki 'Nitelikler' düğmesine tıklayabilirsin.",
		],
		"npc.speech.villager_under_attack": [
    		"Saldırı altındayım, yardım edin!",
			"Saldırı altındayım! Yardım edin!",
			"Ahhh! Saldırı altındayım!",
			"Ahhh! Saldırı altındayım! Yardım edin!",
			"Saldırı altındayım! Bana yardım edin!",
			"Yardım edin! Saldırı altındayım!",
			"Bana yardım edin! Saldırı altındayım!",
			"Yardım edin!",
			"Yardım edin! Yardım edin!",
			"Yardım edin! Yardım edin! Yardım edin!",
			"Saldırı altındayım!",
			"AAAHHHH! Saldırı altındayım!",
			"AAAHHHH! Yardım edin! Saldırı altındayım!",
			"Saldırı altındayız! Yardım edin!",
			"Katil! Yardım edin!",
			"Bir katil serbestçe dolaşıyor! Yardım edin!",
			"Beni öldürmeye çalışıyorlar! Yardım edin!",
			"Gardiyanlar, saldırı altındayım!",
			"Saldırı altındayım! Gardiyanlar!",
			"Gardiyanlar! Saldırı altındayım!",
			"Saldırı altındayım! Gardiyanlar! Yardım edin!",
			"Gardiyanlar! Çabuk gelin!",
			"Gardiyanlar! Gardiyanlar!",
			"Bana saldıran bir kötü var! Yardım edin!",
			"Gardiyanlar, bu pis kötüyü öldürün!",
			"Gardiyanlar! Burada bir katil var!",
			"Gardiyanlar! Bana yardım edin!",
			"Bu yanına kalmayacak! Gardiyanlar!",
			"Seni şeytan!",
			"Bana yardım edin!",
			"Lütfen! Yardım edin!",
			"Ahhh! Gardiyanlar! Yardım edin!",
			"Benim için geliyorlar!",
			"Yardım edin! Yardım edin! Baskı altındayım!",
			"Ah, artık sistemin doğasında var olan şiddeti görüyoruz.",
			"Bu bana göre bir çizik bile değil!",
            "Yapma şunu!",
            "Ben sana ne yaptım ki?!",
            "Lütfen bana saldırmayı kes!",
            "Hey! Onu nereye yönelttiğine dikkat et!",
            "Aşağılık herif, gözüm bile görmesin!",
            "Durdur şunu! Git buradan!",
            "Şimdi beni kızdırmaya başladın!",
            "Hey! Sen kim olduğunu zannediyorsun ki?!",
            "Bunun için kelleni alacağım!",
            "Yapma, lütfen! Değerli hiçbir şeyim yok bile!",
            "Kardeşimi üzerine salacağım, o benden bile büyük!",
            "Olamaaaz, Seni anneme söyleyeceğim!",
            "Lanet olsun sana!",
            "Lütfen yapma şunu.",
            "Bunu yapman pek kibarca değildi!",
            "Evet silahın çalışıyor, şimdi kaldırabilir misin?",
            "Bağışlayın beni!",
            "Lütfen, benim bir ailem var!",
            "Ölmek için çok gencim!",
            "Bunu konuşarak çözebilir miyiz?",
            "Şiddet hiçbir zaman çare değildir!",
            "Günüm gittikçe kötüleşiyor...",
            "Hey, bu acıttı!",
            "Eek!",
            "Ne kadar da kaba!",
            "Dur, sana yalvarırım!",
            "Lanet olsun sana!",
            "Bu eğlenceli bile değil.",
            "Ne cürret?!",
            "Bunu sana ödeteceğim!",
            "Yapmaya devam edersen bunun için pişman olacaksın!",
            "Sana zarar vermek zorunda bırakma beni!",
            "Bir yanlış anlaşılma olmalı!",
            "Bunu yapmak zorunda değilsin!",
            "Defol!",
            "Bu gerçekten acıttı!",
            "Bunu neden yaptın ki?",
            "Ruhlar tarafından, dur!",
            "Beni başkasıyla karıştırmış olmalısın!",
            "Bunu haketmiyorum!",
            "Lütfen bunu bir daha yapma.",
            "Muhafızlar, şu canavarı göle atın!",
            "Tarrasque'ımı üzerine salarım!",
		],
    }
)
